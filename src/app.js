const express = require("express");
const bodyParser = require("body-parser");

const app = express();
const server = require("http").Server(app);
var io = require("socket.io")(server)

io.on('connection', function(socket){
    console.log("un Cliente se ha conectado", socket.id);

    /*socket.on("hola", data => console.log(data))
    socket.on("sweet", data => console.log(data))*/

    socket.on("id", data => {
        socket.join(data)
    })

    socket.on("message", data => {
        console.log(data)
        io.to(data.to).emit("messages", data);
        //io.to(data.from).emit("messages", data);
    })

})


const routesUser = require("./api/user");   
const routesMessage = require("./api/message");
const routesContact = require("./api/contact");
const routesSala = require("./api/sala");


const cors = require("./middlewares/access_cors");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors);
// Rutas de la api
app.use('/api/', routesUser);
app.use('/apiMessage/', routesMessage);
app.use('/apiContact/', routesContact);
app.use('/apiSala/', routesSala);


module.exports = server;