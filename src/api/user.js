const express = require("express");
const userCtrl = require("../controllers/userController");
const ensureTokenMiddleware = require("../middlewares/ensuretoken");


const Router = express.Router();

Router.post("/login", userCtrl.login)
      .post("/register", userCtrl.register)
      .get("/", userCtrl.usuarios)
      .get("/secure", ensureTokenMiddleware,  userCtrl.secure)
      .delete("/remove", userCtrl.remove)

module.exports = Router;