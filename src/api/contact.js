const express = require("express");
const contactCtrl = require("../controllers/contactController");
const ensureTokenMiddleware = require("../middlewares/ensuretoken"); 

const Router = express.Router();

Router.get("/contactos", contactCtrl.contactos)
      .post("/nuevo/contacto", contactCtrl.nuevo)
      .delete("/remove", contactCtrl.remove)
      .get("/contactoPorId/:id", ensureTokenMiddleware, contactCtrl.contactoPorId)

module.exports = Router;