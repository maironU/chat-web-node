const express = require("express");
const salaCtrl = require("../controllers/salaController");


const Router = express.Router();

Router.get("/mensajes/:from/:to", salaCtrl.mensajes)
      .post("/nueva/sala", salaCtrl.nuevo)
      .delete("/remove", salaCtrl.remove)


module.exports = Router;