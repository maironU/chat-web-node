const express = require("express");
const messageCtrl = require("../controllers/messageController");


const Router = express.Router();

Router.get("/mensajes/:from/:to", messageCtrl.mensajes)
      .post("/nuevo/mensaje", messageCtrl.nuevo)
      .delete("/remove", messageCtrl.remove)

module.exports = Router;