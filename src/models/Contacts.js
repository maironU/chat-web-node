const mongoose = require("mongoose")

const Schema = mongoose.Schema;

const ContactSchema = ({
    idUser: { type: Schema.Types.ObjectId, ref: "User"},
    idContact: { type: Schema.Types.ObjectId, ref: "User"}
});

const Contact = mongoose.model("Contact", ContactSchema);

module.exports = Contact;