const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const User = require("./Users");

const MessageSchema = new Schema({
    idFrom: { type: Schema.Types.ObjectId, ref: "User"},
    idTo: { type: Schema.Types.ObjectId, ref: "User" },
    message: { type: String},
    idSala: { type: String },
    date: { type: Date}
})

const Messages = mongoose.model('Messages', MessageSchema);


module.exports = Messages;