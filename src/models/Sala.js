const mongoose = require("mongoose")

const Schema = mongoose.Schema

const SalaSchema = ({
    idSala: {type: String},
    fromId: {type: String},
    mensaje: {type: String},
    date: { type: Date }
})

const Sala = mongoose.model("Sala", SalaSchema)

module.exports = Sala;