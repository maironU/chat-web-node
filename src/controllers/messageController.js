const Message = require("../models/Messages");

function mensajes(req, res){
    Message.find({idSala: req.params.from + req.params.to})
            .populate('idFrom')
           .populate('idTo')
           .then((mensajes) => {
               if(mensajes.length > 0){
                    console.log("entre en la primera: ", req.params.from + req.params.to)
                    res.json(mensajes)
               }else{
                    Message.find({idSala: req.params.to + req.params.from})
                    .populate('idFrom')
                    .populate('idTo')
                    .then((mensajes) => {
                    console.log("entre en la segunda: ", req.params.to + req.params.from)

                        res.json(mensajes)
                    })
                }
            })
}

/*function messageById(req, res){
    console.log("to", req.params.to)
    console.log("from", req.params.from)

    Message.find({idFrom: req.params.from, idTo: req.params.to})
           .populate('idFrom')
           .populate('idTo')
            .then(mensajes => {
                res.json(mensajes)
            })
}
*/
function remove(req, res){
    Message.remove({}, function(err, mensaje){
        res.json({message: "eliminado"})
    })
}

function nuevo(req, res){
    const from = req.body.idFrom;
    const to = req.body.idTo;
    console.log(from + to)

    Message.find({idSala: from + to, idSala: to + from})
        .then(response => {
            if(response.length === 0){
                const message = new Message({
                    idFrom: from,
                    idTo: to,
                    message: req.body.message,
                    idSala: from + to,
                    date: new Date()
                })
                message.save()
                        .then(message => {
                            res.json(message)
                        })
                        .catch(error => console.log(error))
            }else{
                const message = new Message({
                    idFrom: from,
                    idTo: to,
                    message: req.body.message,
                    idSala: response[0].idSala,
                    date: new Date()
                })
                message.save()
                        .then(message => {
                            console.log(message)
                            res.json(message)
                        })
                        .catch(error => console.log(error))
            }
        })
}

module.exports = {
    mensajes,
    nuevo,
    remove,
}