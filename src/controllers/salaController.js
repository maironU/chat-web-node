const Sala = require("../models/Sala");

function mensajes(req, res){
    Sala.find({idSala: req.params.idSala})
           .exec((err, mensajes) => {
                res.json(mensajes)
           })
}

function remove(req, res){
    Sala.remove({}, function(err, sala){
        res.json({message: "eliminado"})
    })
}

function nuevo(req, res){
    const from = req.body.idFrom;
    const to = req.body.idTo;
    console.log(from+ to)

    Sala.find({idSala: from + to, idSala: to + from})
        .then(response => {
            console.log("veamos si exiet: ", response[0])
            if(response.length === 0){
                const sala = new Sala({
                    idSala: from + to,
                    fromId: from,
                    mensaje: req.body.message,
                    date: new Date()
                })
                sala.save()
                        .then(sala => {
                            res.json(sala)
                        })
                        .catch(error => console.log(error))
            }else{
                console.log("ya existe sala")
                const sala = new Sala({
                    idSala: response[0].idSala,
                    fromId: from,
                    mensaje: req.body.message,
                    date: new Date()
                })
                sala.save()
                        .then(sala => {
                            console.log(sala)
                            res.json(sala)
                        })
                        .catch(error => console.log(error))
            }
        })

    
}

module.exports = {
    mensajes,
    nuevo,
    remove
}