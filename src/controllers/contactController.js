const Contact = require("../models/Contacts");
const jwt = require("jsonwebtoken");
const secret = require("../config/clave");

function contactos(req, res){
    Contact.find({})
           .populate('idUser')
           .populate('idContact')
           .exec((err, contactos) => {
                res.json(contactos)
           })
}

function contactoPorId(req, res){

    jwt.verify(req.token, secret.secret, (error, data) => {
        if(error) return res.sendStatus(403)
        Contact.find({idUser: req.params.id})
           .populate('idContact')
           .exec((err, contactos) => {
               res.json(contactos)
           })
    })
}

function remove(req, res){
    Contact.remove({}, function(err, contactos){
        res.json({message: "eliminado"})
    })
}

function nuevo(req, res){

    const contacto = new Contact(req.body);
    contacto.save()
            .then(contacto => console.log(contacto))
            .catch(error => console.log(error))
}

module.exports = {
    contactos,
    nuevo,
    remove,
    contactoPorId
}