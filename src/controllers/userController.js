const User = require("../models/Users");
const jwt = require("jsonwebtoken");
const secret = require("../config/clave");

function login(req, res){
    var user = req.body.number;
    var password = req.body.password;

    User.findOne({number: user, password: password})
        .then(user => {
            if(!user) return res.json();

            var token = jwt.sign({user}, secret.secret, { expiresIn: '24h'});

            res.json({
                token
            })

        }).catch(error => {
            if(error) return res.json(error);
        })
}

function remove(req, res){
    User.remove({}, function(err, user){
        res.json({message: "eliminado"})
    })
}

function register(req, res){
    let user = new User(req.body);
    user.save()
        .then(user => res.json({message: "creado"}))
        .catch(error => res.json(error));
}

function usuarios(req, res){
    console.log(new Date())
    User.find({})
        .then(users => {
            if(users) return res.json(users)
            return res.json({message: "No hay usuarios"});
        }).catch(error => {
            if(error) return res.json(error)
        })
}

function secure(req, res){
    jwt.verify(req.token, secret.secret, (error, data) => {
        if(error) return res.sendStatus(403)
        res.json(data)
    })
}

module.exports = {
    login,
    register,
    usuarios,
    secure,
    remove
}
