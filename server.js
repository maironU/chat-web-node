const Database = require("./src/config/database");
const CONFIG = require("./src/config/config");
const server = require("./src/app");

Database.connect();

server.listen(CONFIG.PORT, error => {
    if(error) return console.log(error);
    console.log("servidor corriendo en el  puerto " + CONFIG.PORT);
})